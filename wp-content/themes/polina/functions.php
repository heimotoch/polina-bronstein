<?php
add_filter('use_block_editor_for_post', '__return_false', 10);
add_filter('use_block_editor_for_post_type', '__return_false', 10);


// require_once(__DIR__ . '/inc/functions/custom_api_endpoints.php');
// require_once(__DIR__ . '/inc/functions/fonts.php');

add_theme_support( 'menus' );


function remove_textarea() {
    remove_post_type_support( 'page', 'editor' );
}

add_action( 'init', 'remove_textarea' );


require_once(__DIR__ . '/inc/functions/enqueue_files.php');



// function my_home_query( $query ) {
//     if ( $query->is_main_query() && !is_admin() ) {
//       $query->set( 'post_type', array( 'blog' ));
//     }
//   }
//   add_action( 'pre_get_posts', 'my_home_query' );



// add_filter('query_vars', function( $vars ){
//     // $vars[] = 'pagename'; 
//     $vars[] = 'category'; 
//     return $vars;
// });