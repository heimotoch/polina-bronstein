

import * as Hammer from 'hammerjs';
import gsap from 'gsap'

const videos = document.querySelectorAll('.video')

videos.forEach((vid, i) => {
    if(!i) {
        vid.style.opacity = 1
    }


    window.addEventListener('scroll', () => {
        const pageY = window.pageYOffset
        const windowHeight = window.innerHeight
        const moduleTop = vid.parentElement.offsetTop - windowHeight

        const logo = document.querySelector('.logo__svg')
        if(pageY > 200) {
            logo.style.width = '6rem'
            logo.parentNode.style.top = '1rem'

        } else {
            logo.style.width = '10rem'
            logo.parentNode.style.top = '4rem'

        }

        if(pageY  > moduleTop) {
            vid.style.opacity = 1
        } else {
            vid.style.opacity = 0
        }
    })
})





    const body = document.querySelector('body')
    const box = document.querySelector('.box')
    const imgs = document.querySelectorAll('.img')
    const nextBtn  = document.querySelector('.box__next')
    const prevBtn  = document.querySelector('.box__prev')
    const closeBtn = document.querySelector('.box__close')




    class Slider {
        constructor() {
            this.isOpen = false,
            this.current = null

            window.addEventListener('keydown', (event) => {
                const key = event.key; // "ArrowRight", "ArrowLeft", "ArrowUp", or "ArrowDown"
                if(key == 'ArrowLeft') {
                    this.prevSlide()
                } else if (key == 'ArrowRight') {
                    this.nextSlide()
                } else if (key == 'Escape') {
                    this.closeBox()
                }
            });
        }

        openBox(e) {
            this.isOpen = true
            box.style.display = 'block'
            body.style.overflowY = 'hidden'
            this.current = ([...imgs].indexOf(e.target))
            this.runSlider(true)
        }

        closeBox() {
            box.style.display = 'none'
            body.style.overflowY = 'scroll'
            this.isOpen = false
        }
        nextSlide() {
            if(this.current === imgs.length - 1) return
            this.current++
            this.runSlider(false)
        }

        prevSlide() {
            if(this.current === 0) return
            this.current = this.current - 1
            this.runSlider(false)
        }

        runSlider(immediately) {
            const duration = immediately ? 0 : 0.4
            gsap.to(document.querySelector('.box__mover'), {duration: duration, x: `${-100 * this.current}vw`})
        }
    
    }


    const slider = new Slider()

    imgs.forEach(el => {
        el.addEventListener('click', (e) => slider.openBox(e))
    })


    nextBtn.addEventListener('click', () => slider.nextSlide())
    prevBtn.addEventListener('click', () => slider.prevSlide())
    closeBtn.addEventListener('click', () => slider.closeBox())

    //HAMMER
    var stage = document.querySelector('.box');

    var hammertime = new Hammer(stage, {});

    hammertime.get('swipe').set({ direction: Hammer.DIRECTION_HORIZONTAL });

    hammertime.on('swipe', ({velocityX}) => {
        if(velocityX > 0) {
            slider.prevSlide();
        } else {
            slider.nextSlide();
        }
    });



    // const projTitle = document.querySelector('.project__title').innerText
    // const sidebarTitles = document.querySelectorAll('.sidebar__title')

    // const sidebarTitlesArr = []

    // sidebarTitles.forEach(el => {
    //     sidebarTitlesArr.push(el.innerText)
    // })

    // const currProj = sidebarTitlesArr.indexOf(projTitle) + 1
    // if(currProj = sidebarTitlesArr.length)
    // document.querySelector('.project__iteration').innerText = `EB1-${currProj}`

