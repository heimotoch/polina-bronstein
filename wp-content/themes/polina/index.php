<?php
$context = Timber::context();
$modules = array();

// if(have_rows('modules')) {
//     while(have_rows('modules')) {
//         the_row();
//         $module_name = get_row_layout();
    
//         switch ($module_name) {
//             case 'text':
//                 $module = array(
//                     'type'			=> $module_name,
//                     'text' => get_sub_field('text'),
//                     );
//             break;

//         }

//         array_push($modules, $module);


//     }
// };

// while ( have_posts() )
//     // Display post
//     if ( have_posts() ) : // If this is the last post, the loop will start over
//         the_row();
//     $module = array(
//         //                     'type'			=> $module_name,
//         //                     'text' => get_sub_field('text'),
//         // 'editor' => wp_editor()
//     );
//     var_dump($module);

//     endif;
// endwhile;

// if ( have_posts() ) : 
//     while ( have_posts() ) : the_post(); 
//         // Display post content
//             $module = array(
//         //                     'type'			=> $module_name,
//         //                     'text' => get_sub_field('text'),
//         'editor' => the_content()
//     );
//     var_dump($module);
//     endwhile; 
// endif; 

$all_imgs = [];

$args = array(
    'posts_per_page'   => -1,
    'post_type'        => 'post',
);
$the_query = new WP_Query( $args );

while ( $the_query->have_posts() ) {
    // go ahead
    $the_query->the_post();
    $elems = [];
    
    if(have_rows('modules')) {
        while(have_rows('modules')) {
            the_row();
            $module_name = get_row_layout();
        
            switch ($module_name) {
                case 'intro':
                    $elem = array(
                        'type'			=> $module_name,
                        'caption' => get_sub_field('caption'),
                        'first_heading' => get_sub_field('first_heading'),
                        'second_heading' => get_sub_field('second_heading'),
                        'text' => get_sub_field('text'),
                        'social_media' => get_sub_field('social_media'),
                        );
                break;
                case 'txt_vid':
                    $elem = array(
                        'type'			=> $module_name,
                        'video' => get_sub_field('video'),
                        'text' => get_sub_field('text'),
                        'is_order_reverse' => get_sub_field('is_order_reverse'),
                        'images_under_video' => get_sub_field('images_under_video'),
                        
                        );
                break;
                case 'images':
                    $images = get_sub_field('images');
                    $elem = array(
                        'type'			=> $module_name,
                        'images' => get_sub_field('images'),  
                        'width' => get_sub_field('width'),                      
                        );

                    foreach($images as $image) {                        
                        array_push($all_imgs, $image['url']);
                    }
                break;

            }

            array_push($elems, $elem);


        }
    };


    $is_vid_as_bg = get_field('vid_as_bg');

    $module = array(
        'type' => 'post',
        'editor' => get_post(get_the_ID())->post_content,
        'is_vid_as_bg' => $is_vid_as_bg,
        'video' => get_field('video'),
        'background' => get_field('background'),
        'padding' => get_field('padding'),
        'max_width' => get_field('max_width'),
        'elems' => $elems
    );

    array_push($modules, $module);

}


    // print_r($modules);

//   die();


// $query = new WP_Query( array( 'post_type' => 'post', 'posts_per_page' => -1 ) );
// $posts = $query->posts;

// foreach($posts as $post) {
//     // the_post();
    
// //     if(have_rows('modules')) {
// //     while(have_rows('modules')) {
// //         the_row();
// //         $module_name = get_row_layout();
    
// //         switch ($module_name) {
// //             case 'img_vid':
// //                 $module = array(
// //                     'type'			=> $module_name,
// //                     'text' => get_sub_field('text'),
// //                     );
// //             break;

// //         }

// //         array_push($modules, $module);


// //     }
// // };






//     $module = array(
//         'editor' => the_content($post->ID)
//     );
//     var_dump($post);

        // array_push($modules, $module);

// }


$context['modules'] = $modules;
$context['images'] = $all_imgs;


Timber::render( 'index.twig', $context ); 


